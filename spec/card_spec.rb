require "spec_helper"

RSpec.describe Card do
  let(:card) { described_class.new("KD") }

  describe "initializer" do
    it "sets the face" do
      expect(card.face).to eq(13)
    end

    it "sets the suit" do
      expect(card.suit).to eq("D")
    end
  end

  describe "to_s" do
    it "returns card origin" do
      expect(card.to_s).to eq("KD")
    end
  end

  describe "ace_high_face" do
    it "returns face for ace_high" do
      card = described_class.new("AC")
      expect(card.ace_high_face).to eq(14)
    end
  end
end
