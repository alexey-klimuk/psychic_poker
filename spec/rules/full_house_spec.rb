require "spec_helper"

RSpec.describe Rules::FullHouse do
  it "has value eq to 6" do
    expect(described_class.value).to eq(6)
  end

  it "has name eq to :full_house" do
    expect(described_class.name).to eq(:full_house)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains three cards with the one face and two with another face" do
      let(:hand) {
        [Card.new("3S"), Card.new("3H"), Card.new("3C"), Card.new("QS"), Card.new("QD")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain three cards with the one face and two with another face" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TH"), Card.new("QS"), Card.new("5S")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
