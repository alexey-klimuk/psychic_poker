require "spec_helper"

RSpec.describe Rules::Flush do
  it "has value eq to 5" do
    expect(described_class.value).to eq(5)
  end

  it "has name eq to :flush" do
    expect(described_class.name).to eq(:flush)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains all cards with the same suit" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TS"), Card.new("QS"), Card.new("5S")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain all cards with the same suit" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TH"), Card.new("QS"), Card.new("5S")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
