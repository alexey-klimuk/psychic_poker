require "spec_helper"

RSpec.describe Rules::ThreeOfAKind do
  it "has value eq to 3" do
    expect(described_class.value).to eq(3)
  end

  it "has name eq to :three_of_a_kind" do
    expect(described_class.name).to eq(:three_of_a_kind)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains three cards with the same face" do
      let(:hand) {
        [Card.new("3S"), Card.new("3T"), Card.new("3C"), Card.new("QS"), Card.new("5S")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain three cards with the same face" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TH"), Card.new("QS"), Card.new("5S")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
