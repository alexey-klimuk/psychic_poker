require "spec_helper"

RSpec.describe Rules::Straight do
  it "has value eq to 4" do
    expect(described_class.value).to eq(4)
  end

  it "has name eq to :straight" do
    expect(described_class.name).to eq(:straight)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains a sequence of cards" do
      let(:hand) {
        [Card.new("3S"), Card.new("5T"), Card.new("4D"), Card.new("7C"), Card.new("6S")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain a sequence of cards" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TH"), Card.new("QS"), Card.new("5S")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
