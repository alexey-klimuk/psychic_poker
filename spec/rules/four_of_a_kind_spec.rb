require "spec_helper"

RSpec.describe Rules::FourOfAKind do
  it "has value eq to 7" do
    expect(described_class.value).to eq(7)
  end

  it "has name eq to :four_of_a_kind" do
    expect(described_class.name).to eq(:four_of_a_kind)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains four cards in different suit" do
      let(:hand) {
        [Card.new("3S"), Card.new("3H"), Card.new("TS"), Card.new("3D"), Card.new("3C")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain four cards in different suit" do
      let(:hand) {
        [Card.new("3S"), Card.new("2H"), Card.new("2S"), Card.new("3D"), Card.new("3C")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
