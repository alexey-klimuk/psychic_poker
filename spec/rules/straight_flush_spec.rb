require "spec_helper"

RSpec.describe Rules::StraightFlush do
  it "has value eq to 8" do
    expect(described_class.value).to eq(8)
  end

  it "has name eq to :straight_flush" do
    expect(described_class.name).to eq(:straight_flush)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains a sequence of cards with the same suit" do
      let(:hand) {
        [Card.new("3S"), Card.new("5S"), Card.new("4S"), Card.new("7S"), Card.new("6S")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain a sequence of cards with the same suit" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TH"), Card.new("QS"), Card.new("5S")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
