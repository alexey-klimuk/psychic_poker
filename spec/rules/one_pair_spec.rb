require "spec_helper"

RSpec.describe Rules::OnePair do
  it "has value eq to 1" do
    expect(described_class.value).to eq(1)
  end

  it "has name eq to :one_pair" do
    expect(described_class.name).to eq(:one_pair)
  end

  describe ".match?" do
    subject { described_class }

    context "when the hand contains one pair" do
      let(:hand) {
        [Card.new("3S"), Card.new("3T"), Card.new("TS"), Card.new("QS"), Card.new("5S")]
      }

      it "returns true" do
        expect(subject.match?(hand)).to eq(true)
      end
    end

    context "when the hand does not contain one pair" do
      let(:hand) {
        [Card.new("3S"), Card.new("6S"), Card.new("TH"), Card.new("QS"), Card.new("5S")]
      }

      it "returns false" do
        expect(subject.match?(hand)).to eq(false)
      end
    end
  end
end
