require "spec_helper"

RSpec.describe Rules::HighCard do
  it "has value eq to 0" do
    expect(described_class.value).to eq(0)
  end

  it "has name eq to :high_card" do
    expect(described_class.name).to eq(:high_card)
  end

  describe ".match?" do
    subject { described_class }

    it "always returns true" do
      expect(subject.match?([])).to eq(true)
    end
  end
end
