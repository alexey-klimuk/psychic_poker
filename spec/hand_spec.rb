require "spec_helper"

RSpec.describe Hand do
  let(:hand) { described_class.new([1,2,3]) }

  describe "initializer" do
    it "assigns cards" do
      expect(hand.cards).to eq([1,2,3])
    end
  end

  describe "check" do
    it "iterates through all rules and call match mathod" do
      Hand::RULES.each do |rule|
        expect(rule).to receive(:match?)
      end
      
      hand.check
    end
  end
end
