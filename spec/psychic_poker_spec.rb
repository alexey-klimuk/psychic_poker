RSpec.describe PsychicPoker do
  it "has a version number" do
    expect(PsychicPoker::VERSION).not_to be nil
  end

  describe "get_best_hand" do
    it "passes all tests properly" do
      expect(STDOUT).to receive(:puts).with("Hand: TH JH QC QD QS Deck: QH KH AH 2S 6S Best hand: straight_flush")
      PsychicPoker.get_best_hand("TH JH QC QD QS QH KH AH 2S 6S")

      expect(STDOUT).to receive(:puts).with("Hand: 2H 2S 3H 3S 3C Deck: 2D 3D 6C 9C TH Best hand: four_of_a_kind")
      PsychicPoker.get_best_hand("2H 2S 3H 3S 3C 2D 3D 6C 9C TH")

      expect(STDOUT).to receive(:puts).with("Hand: 2H 2S 3H 3S 3C Deck: 2D 9C 3D 6C TH Best hand: full_house")
      PsychicPoker.get_best_hand("2H 2S 3H 3S 3C 2D 9C 3D 6C TH")

      expect(STDOUT).to receive(:puts).with("Hand: 2H AD 5H AC 7H Deck: AH 6H 9H 4H 3C Best hand: flush")
      PsychicPoker.get_best_hand("2H AD 5H AC 7H AH 6H 9H 4H 3C")

      expect(STDOUT).to receive(:puts).with("Hand: AC 2D 9C 3S KD Deck: 5S 4D KS AS 4C Best hand: straight")
      PsychicPoker.get_best_hand("AC 2D 9C 3S KD 5S 4D KS AS 4C")

      expect(STDOUT).to receive(:puts).with("Hand: KS AH 2H 3C 4H Deck: KC 2C TC 2D AS Best hand: three_of_a_kind")
      PsychicPoker.get_best_hand("KS AH 2H 3C 4H KC 2C TC 2D AS")

      expect(STDOUT).to receive(:puts).with("Hand: AH 2C 9S AD 3C Deck: QH KS JS JD KD Best hand: two_pairs")
      PsychicPoker.get_best_hand("AH 2C 9S AD 3C QH KS JS JD KD")

      expect(STDOUT).to receive(:puts).with("Hand: 6C 9C 8C 2D 7C Deck: 2H TC 4C 9S AH Best hand: one_pair")
      PsychicPoker.get_best_hand("6C 9C 8C 2D 7C 2H TC 4C 9S AH")

      expect(STDOUT).to receive(:puts).with("Hand: 3D 5S 2H QD TD Deck: 6S KH 9H AD QH Best hand: high_card") 
      PsychicPoker.get_best_hand("3D 5S 2H QD TD 6S KH 9H AD QH")
    end
  end
end
