# Psychic Poker

Test assignment https://github.com/btccom/Hire/blob/master/exercise/psychic-poker-en.md


# Install

`$ git clone git@bitbucket.org:alexey-klimuk/psychic_poker.git`

`$ bundle`

# Usage

`$ bin/console`

`> PsychicPoker.get_best_hand("2H 2S 3H 3S 3C 2D 3D 6C 9C TH") => "Hand: 2H 2S 3H 3S 3C Deck: 2D 3D 6C 9C TH Best hand: four_of_a_kind"`

# Specs

`$ rspec spec`
