require "psychic_poker/version"
require "psychic_poker/card"
require "psychic_poker/analizer"

module PsychicPoker
  module_function

  def get_best_hand(line)
    cards = line.split(" ").map { |item| Card.new(item) }
    hand = cards[0..4]
    deck = cards[5..9]
    best_hand = Analizer.call(hand, deck)
    puts "Hand: #{hand.join(" ")} Deck: #{deck.join(" ")} Best hand: #{best_hand}"
  end
end
