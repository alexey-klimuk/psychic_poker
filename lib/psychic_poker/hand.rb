require "psychic_poker/rules/flush"
require "psychic_poker/rules/four_of_a_kind"
require "psychic_poker/rules/full_house"
require "psychic_poker/rules/high_card"
require "psychic_poker/rules/one_pair"
require "psychic_poker/rules/straight_flush"
require "psychic_poker/rules/straight"
require "psychic_poker/rules/three_of_a_kind"
require "psychic_poker/rules/two_pairs"

class Hand
  attr_reader :cards

  RULES = [Rules::Flush, Rules::FourOfAKind, Rules::FullHouse, Rules::HighCard,
           Rules::OnePair, Rules::StraightFlush, Rules::Straight,
           Rules::ThreeOfAKind, Rules::TwoPairs].freeze

  def initialize(cards)
    @cards = cards
  end

  def check
    RULES.select { |rule| rule.match?(cards) }.sort_by(&:value).last
  end
end
