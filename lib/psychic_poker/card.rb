class Card
  attr_reader :face, :suit

  FACE_VALUES = {
    'A' => 1,
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    '7' => 7,
    '8' => 8,
    '9' => 9,
    'T' => 10,
    'J' => 11,
    'Q' => 12,
    'K' => 13
  }.freeze

  def initialize(card)
    @origin = card
    @face = FACE_VALUES[card.chars.first]
    @suit = card.chars.last
  end

  def ace_high_face
    face == 1 ? 14 : face
  end

  def to_s
    @origin
  end
end
