require "psychic_poker/hand"

module Analizer
  module_function

  def call(hand, deck)
    best_hand = Rules::HighCard
    deck_size = deck.size
    hand_size = hand.size

    (0..deck_size).each do |i|
      hand.combination(hand_size - i).each do |combination|
        cur_hand = Hand.new(combination + deck[0...i]).check
        best_hand = cur_hand if cur_hand.value > best_hand.value
      end
    end

    best_hand.name
  end
end
