module Rules
  module FullHouse
    module_function

    def match?(hand)
      grouped = hand.group_by { |card| card.face }
      grouped.size == 2 && grouped.values.map(&:size).sort == [2, 3]
    end

    def value
      6
    end

    def name
      :full_house
    end  
  end
end
