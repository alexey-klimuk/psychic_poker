module Rules
  module FourOfAKind
    module_function

    def match?(hand)
      hand.group_by { |card| card.face }.select { |_, cards| cards.size == 4 }.any?
    end

    def value
      7
    end

    def name
      :four_of_a_kind
    end
  end
end
