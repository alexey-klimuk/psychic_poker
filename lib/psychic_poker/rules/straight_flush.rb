module Rules
  module StraightFlush
    module_function

    def match?(hand)
      Straight.match?(hand) && Flush.match?(hand)
    end

    def value
      8
    end

    def name
      :straight_flush
    end
  end
end
