module Rules
  module ThreeOfAKind
    module_function

    def match?(hand)
      hand.group_by { |card| card.face }.select { |_, cards| cards.size == 3 }.any?
    end

    def value
      3
    end

    def name
      :three_of_a_kind
    end  
  end
end
