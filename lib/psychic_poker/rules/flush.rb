module Rules
  module Flush
    module_function

    def match?(hand)
      hand.group_by { |card| card.suit }.size == 1
    end

    def value
      5
    end

    def name
      :flush
    end
  end
end
