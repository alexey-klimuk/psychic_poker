module Rules
  module TwoPairs
    module_function

    def match?(hand)
      hand.group_by { |card| card.face }.select { |_, cards| cards.size == 2 }.size == 2
    end

    def value
      2
    end

    def name
      :two_pairs
    end  
  end
end
