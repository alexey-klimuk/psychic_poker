module Rules
  module HighCard
    module_function

    def match?(hand)
      true
    end

    def value
      0
    end

    def name
      :high_card
    end  
  end
end
