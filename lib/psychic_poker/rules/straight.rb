module Rules
  module Straight
    module_function

    def match?(hand)
      high_five = hand.sort_by { |card| card.face }
                      .each_cons(2)
                      .all? { |x,y| y.face == x.face + 1 }
      ace_high = hand.sort_by { |card| card.ace_high_face }
                     .each_cons(2)
                     .all? { |x,y| y.ace_high_face == x.ace_high_face + 1 }
      high_five || ace_high
    end

    def value
      4
    end

    def name
      :straight
    end
  end
end
