module Rules
  module OnePair
    module_function

    def match?(hand)
      hand.group_by { |card| card.face }.select { |_, cards| cards.size == 2 }.any?
    end

    def value
      1
    end

    def name
      :one_pair
    end  
  end
end
